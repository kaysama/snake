const merge = require('webpack-merge')
const publish = require('./webpack.publish')
const webpack = require('webpack')

console.log('=======building for pre-publish environment========')

module.exports = merge(publish, {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('pre')
    }),
  ]
})
