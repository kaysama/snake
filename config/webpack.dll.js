const path = require('path')
const webpack = require('webpack')

const vendors = ['axios']
module.exports = {
  mode: 'production',
  entry: {
    vendor: vendors
  },
  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name]_[hash].js',
    library: '[name]_[hash]'
  },
  plugins: [
    new webpack.DllPlugin({
      context: __dirname,
      name: '[name]_[hash]', // same as output.library,
      path: path.join(__dirname, '../dist', 'manifest.json')
    })
  ]
}
