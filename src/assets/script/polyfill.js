/**
 * Created by Chen Kai on 2017/11/20.
 */
import { Base64 } from './util'

window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame

/**
 * Base64编/解码
 * @type {{characters: string, encode: Base64.encode, decode: Base64.decode}}
 */

window.btoa = Base64.encode
window.atob = Base64.decode
