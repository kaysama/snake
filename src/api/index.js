/**
 * Created by ChenKai on 2018/03/15.
 */

'use strict'

import fetch from './fetch'

export default {

  auth (room) {
    const req = {
      url: '/auth',
      method: 'post',
      query: {room: room},
      timeout: 0,
    }
    return fetch(req)
  },

  logout () {
    return fetch({
      url: '/logout',
      method: 'get',
      headers: {
        Authorization: 'Basic xxx'
      }
    })
  }
}
