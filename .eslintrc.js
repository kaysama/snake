// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  rules: {
    'arrow-parens': 'off',  //  This rule enforces parentheses around arrow function parameters regardless of arity
    'no-useless-escape': 'off',  // This rule flags escapes that can be safely removed without changing behavior
    'accessor-pairs': 'off',
    'padded-blocks': 'off',
    'comma-dangle': 'off',
    'spaced-comment': 'off',
    'comma-spacing': 'off',
    'brace-style': 'off',
    'block-spacing': 'off',
    'prefer-promise-reject-errors': 'off',
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-new': 'off',  // allow new
    'eol-last': 'warn',
    'no-unused-vars': 'warn',
    'no-trailing-spaces': 'warn',
    'no-multiple-empty-lines': 'warn',
    'camelcase': 'off',
  },
}
